﻿namespace Z.KM.ECS
{
    public interface ISystem
    {
        void AddObject(IComponentHolder gameObject);
        void RemoveObject(IComponentHolder gameObject);
        void Update();
    }
}
