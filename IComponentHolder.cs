﻿namespace Z.KM.ECS
{
    public interface IComponentHolder
    {
        TComponentType GetComponent<TComponentType>() where TComponentType : class, IComponent;
    }
}
