﻿using System.Collections.ObjectModel;

namespace Z.KM.ECS.BaseSystems
{
    /// <summary>
    /// базовый класс для системы, которая каждый Update вызывает UpdateObjects
    /// всем принятым объектам, для каждого объекта с каждым, один раз для каждой пары.
    /// </summary>
    public abstract class AbstractNodeToNodeSystem<TNode> : AbstractNodeSystem<TNode>
    {

        protected override void UpdateNodes(ReadOnlyCollection<TNode> nodes)
        {
            var count = nodes.Count;
            for (var i = 0; i < count; i++)
            {
                var item1 = nodes[i];
                for (var j = i + 1; j < count; j++)
                {
                    UpdateObjects(item1, nodes[j]);
                }
            }
        }

        protected abstract void UpdateObjects(TNode a, TNode b);
    }
}
