﻿using System.Collections.ObjectModel;

namespace Z.KM.ECS.BaseSystems
{
    /// <summary>
    /// базовая система, которая предлагает конвертировать объекты в TNode
    /// и работать уже с ними в Update
    /// </summary>
    public abstract class AbstractNodeSystem<TNode> : ISystem, ObjectToNodeFilter<TNode>.INodeCreator
    {
        private readonly ObjectToNodeFilter<TNode> _filter;

        protected AbstractNodeSystem()
        {
            _filter = new ObjectToNodeFilter<TNode>(this);
        }


        public void AddObject(IComponentHolder gameObject)
        {
            _filter.AddObject(gameObject);
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            _filter.RemoveObject(gameObject);
        }

        public void Update()
        {
            UpdateNodes(_filter.Output);
        }

        protected abstract void UpdateNodes(ReadOnlyCollection<TNode> nodes);
        protected abstract bool ConvertToNode(IComponentHolder gameObject, out TNode result);

        bool ObjectToNodeFilter<TNode>.INodeCreator.ConvertToNode(IComponentHolder gameObject, out TNode result)
        {
            return ConvertToNode(gameObject, out result);
        }
    }
}
