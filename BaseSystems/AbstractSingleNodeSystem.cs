﻿using System.Collections.ObjectModel;

namespace Z.KM.ECS.BaseSystems
{
    /// <summary>
    /// базовая система, позволяющая принять GameObject,
    /// с помощью ConvertToNode отфильтровать объекты, нужные компоненты сгруппировать в TNode
    /// а затем обновлять ноды по одному.
    /// </summary>
    public abstract class AbstractSingleNodeSystem<TNode> : AbstractNodeSystem<TNode>
    {
        protected override void UpdateNodes(ReadOnlyCollection<TNode> objects)
        {
            for (var i = 0; i < objects.Count; i++)
            {
                UpdateObject(objects[i]);
            }
        }

        protected abstract void UpdateObject(TNode node);
    }
}
