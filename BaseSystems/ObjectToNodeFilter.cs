﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Z.KM.ECS.BaseSystems
{
    /// <summary>
    /// класс, который принимает добавление и удаление "объектов",
    /// фильтрует и конвертирует их в "ноды",
    /// и на выходе имеет немодифицируемую коллекцию нод.
    /// </summary>
    public sealed class ObjectToNodeFilter<TNode>
    {

        public interface INodeCreator
        {
            bool ConvertToNode(IComponentHolder gameObject, out TNode result);
        }

        private class ObjectWithNode
        {
            public IComponentHolder Object;
            public TNode Node;
        }

        private readonly INodeCreator _nodeCreator;
        private readonly List<ObjectWithNode> _objects;
        private readonly List<TNode> _nodes;
        public readonly ReadOnlyCollection<TNode> Output;

        public ObjectToNodeFilter(INodeCreator nodeCreator)
        {
            _nodeCreator = nodeCreator;
            _objects = new List<ObjectWithNode>();
            _nodes = new List<TNode>();
            Output = _nodes.AsReadOnly();
        }


        public void AddObject(IComponentHolder gameObject)
        {
            TNode node;
            if (!_nodeCreator.ConvertToNode(gameObject, out node))
            {
                return;
            }
            var objectWithNode = new ObjectWithNode();
            objectWithNode.Object = gameObject;
            objectWithNode.Node = node;
            _objects.Add(objectWithNode);
            _nodes.Add(node);
        }

        public bool RemoveObject(IComponentHolder gameObject)
        {
            for (var i = 0; i < _objects.Count; i++)
            {
                if (_objects[i].Object == gameObject)
                {
                    _objects.RemoveAt(i);
                    _nodes.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
    }
}
