﻿using System.Collections.Generic;

namespace Z.KM.ECS
{
    public sealed class ECSCore
    {

        private readonly List<IComponentHolder> _objects;
        private readonly List<ISystem> _systems;

        public ECSCore()
        {
            _objects = new List<IComponentHolder>();
            _systems = new List<ISystem>();
        }

        public void AddSystem(ISystem system)
        {
            _systems.Add(system);
            for (var i = 0; i < _objects.Count; i++)
            {
                system.AddObject(_objects[i]);
            }
        }

        public void AddObject(IComponentHolder gameObject)
        {
            _objects.Add(gameObject);
            for (var i = 0; i < _systems.Count; i++)
            {
                _systems[i].AddObject(gameObject);
            }
        }

        public void RemoveObject(IComponentHolder gameObject)
        {
            if (!_objects.Remove(gameObject))
            {
                return;
            }
            for (var i = 0; i < _systems.Count; i++)
            {
                _systems[i].RemoveObject(gameObject);
            }
        }

        public void Update()
        {
            for (var i = 0; i < _systems.Count; i++)
            {
                _systems[i].Update();
            }
        }
    }
}
