﻿using System;
using System.Collections.Generic;

namespace Z.KM.ECS
{
    public sealed class GameObject : IComponentHolder
    {

        private readonly Dictionary<Type, IComponent> _components = new Dictionary<Type, IComponent>();

        public TComponentType GetComponent<TComponentType>() where TComponentType : class, IComponent
        {
            IComponent result;
            if (_components.TryGetValue(typeof(TComponentType), out result))
            {
                return (TComponentType)result;
            }
            return null;
        }

        public void AddComponent<TComponentType>(TComponentType component) where TComponentType : IComponent
        {
            if (_components.ContainsKey(typeof(TComponentType)))
            {
                throw new Exception("already contains component of type " + typeof(TComponentType));
            }
            _components[typeof(TComponentType)] = component;
        }
    }
}
